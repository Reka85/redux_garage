import React, {Component} from "react";
import { connect } from "react-redux";
import { reduxForm, Field } from "redux-form";
//import { Link } from "react-router-dom";
//import { addCar } from "../actions";
//import NavBar from "../components/nav_bar";
import PropTypes from "prop-types";


const required = value => value ? undefined : 'Required'

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type}/>
      {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
  </div>
)
const FieldLevelValidationForm = (props) => {

  const { handleSubmit, pristine, reset, submitting } = props
  return (
    <form onSubmit={handleSubmit}>
      <Field name="brand" type="text"
        component={renderField} label="brand"
        validate={[ required ]}
      />
      <Field name="model" type="text"
        component={renderField} label="model"
        validate={required}

      />
      <Field name="owner" type="text"
        component={renderField} label="owner"
        validate={[ required]}

      />
      <Field name="plate" type="text"
        component={renderField} label="plate"
        validate={[ required]}

      />
      <div>
        <button type="submit" disabled={submitting}>Submit</button>
        <button type="button" disabled={pristine || submitting} onClick={reset}>Clear Values</button>
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'fieldLevelValidation'
})(FieldLevelValidationForm);

